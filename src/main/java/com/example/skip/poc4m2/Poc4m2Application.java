package com.example.skip.poc4m2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * Prof of concept application
 */
@SpringBootApplication
@EnableJms
public class Poc4m2Application {
/**
 * Root method
 * @param args command line parameters
 */
public static void main(final String[] args) {
SpringApplication.run(Poc4m2Application.class, args);
}
}
