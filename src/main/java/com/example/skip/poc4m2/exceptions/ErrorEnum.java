package com.example.skip.poc4m2.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Error enum
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum ErrorEnum {
    JSON_MARSHALLING_ERROR("Error haciendo marshalling del Json."),
    JMS_ERROR("Java message service error.");

    private String msg;

}
