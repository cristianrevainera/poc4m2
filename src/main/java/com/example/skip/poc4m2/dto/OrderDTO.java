package com.example.skip.poc4m2.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Order entity DTO
 */


@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class OrderDTO {
    @Getter
    @Setter
    private String email;

}
