package com.example.skip.poc4m2.controller;

import com.example.skip.poc4m2.dto.ProductDTO;
import com.example.skip.poc4m2.exceptions.ErrorEnum;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.skip.poc4m2.dto.Operation;
import com.example.skip.poc4m2.exceptions.ErrorWrapper;
import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ProductControllerTest {

    private String product1Name;
    private String product2Name;
    private String product3Name;
    private ProductDTO product1;
    private ProductDTO product2;
    private ProductDTO product3;
    private List<ProductDTO> productDTOs;
    private JmsTemplate jmsTemplateMock;
    private ObjectMapper objectMapperMock;
    private ProductController productController;

    private final String topicName = "jms.message.topic1";

    @Before
    public void setup() {
        product1Name = "Phone";
        product1 = new ProductDTO();
        product1.setName(product1Name);
        product1.setOperation(Operation.INSERT.name());

        product2Name = "Tablet";
        product2 = new ProductDTO();
        product2.setName(product2Name);
        product2.setOperation(Operation.UPDATE.name());

        product3Name = "PC";
        product3 = new ProductDTO();
        product3.setName(product2Name);
        product3.setOperation(Operation.DELETE.name());

        productDTOs = Arrays.asList(product1, product2, product3);

        jmsTemplateMock = mock(JmsTemplate.class);
        objectMapperMock = mock(ObjectMapper.class);

        productController = new ProductController(jmsTemplateMock, objectMapperMock);
    }

    @Test
    public void saveProducts() {


        doNothing().when(jmsTemplateMock).convertAndSend(topicName, productDTOs);

        ResponseEntity responseEntity = productController.saveProducts(productDTOs);

        assertEquals(responseEntity.getStatusCode().value(), HttpStatus.OK.value());
    }

    @Test
    public void saveProductsJsonMarshallFails() throws JsonProcessingException {

        JsonProcessingException jsonProcessingException = mock(JsonProcessingException.class);

        doThrow(jsonProcessingException).when(objectMapperMock).writeValueAsString(product1);

        ResponseEntity productResponseEntity = productController.saveProducts(productDTOs);
        ErrorWrapper errorWrapper = (ErrorWrapper) productResponseEntity.getBody();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), productResponseEntity.getStatusCode().value());
        assertEquals(ErrorEnum.JSON_MARSHALLING_ERROR.getMsg(), errorWrapper.getMessage());

     }

    @Test
    public void saveProductsJMSFails() throws JsonProcessingException {
        JmsException jmsException = mock(JmsException.class);

        Whitebox.setInternalState(productController, "topicName", topicName);

        doReturn("{}").when(objectMapperMock).writeValueAsString(product1);
        doThrow(jmsException).when(jmsTemplateMock).convertAndSend( topicName, "{}");
        ResponseEntity productResponseEntity = productController.saveProducts(productDTOs);
        ErrorWrapper errorWrapper = (ErrorWrapper) productResponseEntity.getBody();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), productResponseEntity.getStatusCode().value());
        assertEquals(ErrorEnum.JMS_ERROR.getMsg(), errorWrapper.getMessage());

    }
}
