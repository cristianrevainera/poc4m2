package com.example.skip.poc4m2.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Product entity DTO
 */
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class ProductDTO {
    private Long id;
    private String name;
    private OrderDTO order;
    private String operation;
}
