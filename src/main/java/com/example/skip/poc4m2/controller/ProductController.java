package com.example.skip.poc4m2.controller;

import com.example.skip.poc4m2.dto.ProductDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.example.skip.poc4m2.exceptions.ErrorEnum;
import com.example.skip.poc4m2.exceptions.ErrorWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Product Controller provides endpoint to add product in bulk to an ActiveMQ topic
 */
@RestController
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    private JmsTemplate jmsTemplate;
    private ObjectMapper objectMapper;

    @Value("${topic.name}")
    private String topicName;

    /**
     *  Product Controller constructor
     * @param jmsTemplate (@link jmsTemplate) jmsTemplate Java message service spring template
     * @param objectMapper (@link objectMapper) objectMapper for marshall
     */
    @Autowired
    public ProductController(final JmsTemplate jmsTemplate, final ObjectMapper objectMapper) {
        this.jmsTemplate = jmsTemplate;
        this.objectMapper = objectMapper;
    }

    /**
     * Post
     * @param productDTOs (@link List<ProductDTO>) productDTO List
     * @return ResponseEntity (@link ResponseEntity) with http status code 200
     */
    @PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity saveProducts(@RequestBody final List<ProductDTO> productDTOs) {
        try {
            for (ProductDTO productDTO : productDTOs) {
                String jsonProductDto = objectMapper.writeValueAsString(productDTO);
                jmsTemplate.convertAndSend(topicName, jsonProductDto);
            }
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (JsonProcessingException jsonProcessingException) {
            LOGGER.error(ErrorEnum.JSON_MARSHALLING_ERROR.getMsg());
            return new ResponseEntity(new ErrorWrapper(ErrorEnum.JSON_MARSHALLING_ERROR.getMsg()), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JmsException e) {
            LOGGER.error(ErrorEnum.JMS_ERROR.getMsg());
            return new ResponseEntity(new ErrorWrapper(ErrorEnum.JMS_ERROR.getMsg()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
