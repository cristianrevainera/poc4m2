package com.example.skip.poc4m2.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Error wrapper
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class ErrorWrapper {
    private String message;
}
